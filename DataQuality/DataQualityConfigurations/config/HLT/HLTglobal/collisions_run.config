# **********************************************************************
# $Id: collisions_run.config 
# **********************************************************************

#######################
# HLT
#######################

#######################
# Output
#######################

output top_level {
    output HLT {

       #Run3 folders 
       output TRHLT {
          output HLT_AllChains {
          }
          output HLT_Electrons {
          }
          output HLT_Gamma {
          }
          output HLT_Muons {
          }
          output HLT_MinBias {
          }
          output HLT_MissingET {
          }
          output HLT_Taus {
          }
          output HLT_Jets {
          }
       #end TRHLT
       }
    #end HLT
    }
#end top_level
}

##############
# References
##############

reference HLT_local_reference {
  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/Collisions/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/Collisions/
  file = data15_13TeV.00267638.physics_EnhancedBias.merge.HIST.r6857_p1831.root
  path = run_267638
  name = same_name
}


#######################
# Histogram Assessments
#######################

#### Run3

dir HLT {
   #output = HLT/TRHLT
   dir ResultMon {
       output = HLT/TRHLT
       hist ConfigConsistency_HLT {
         algorithm = HLT_Histogram_Empty
         description = All bins should be empty. If any bin is filled, inform the Trigger ONLINE Expert IMMEDIATELY. 
       }

       hist HLTEvents { 
	 algorithm = HLT_Histogram_Not_Empty&GatherData
	 description = A simple count histograms for the number of events passing the HLT. 
       }

       hist L1Events { 
         algorithm = HLT_Histogram_Not_Empty&GatherData
	 description = On the x-axis you can find all L1 items, with event counts (on y-axis). This histogram should not be empty. 
       }

       dir HLT_AllChains {
 	output = HLT/TRHLT/HLT_AllChains
	algorithm = HLT_Histogram_Not_Empty&GatherData
        hist HLT_AllChainsPS {
          description = On the x-axis you can find all PRESCALED HLT chains, with event counts (on y-axis). This histogram might be empty. 
	}
	hist HLT_AllChainsRAW {
          description = On the x-axis you can find all HLT chains, with event counts (on y-axis). This histogram should not be empty. 
	}
	hist HLT_AllChainsRoIs {
          description = eta/phi map for HLT RoIs. Histogram should not be empty. If it is, contact the Trigger Offline Expert on-call. 
	}
       }

       dir HLT_Electrons {
 	output = HLT/TRHLT/HLT_Electrons
	algorithm = HLT_Histogram_Not_Empty&GatherData
        hist HLT_ElectronsPS {
          description = On the x-axis you can find all PRESCALED HLT electron chains, with event counts (on y-axis). This histogram might be empty. 
	}
	hist HLT_ElectronsRAW {
          description = On the x-axis you can find all HLT electron chains, with event counts (on y-axis). This histogram should not be empty. 
	}
	hist HLT_ElectronsRoIs {
          description = eta/phi map for electron trigger RoIs. Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Egamma Expert on-call. 
	}
       }

      dir HLT_Gamma {
 	output = HLT/TRHLT/HLT_Gamma
   	algorithm = HLT_Histogram_Not_Empty&GatherData
	hist HLT_GammaPS {
          description = On the x-axis you can find all PRESCALED HLT gamma chains, with event counts (on y-axis). This histogram might be empty.
	}
	hist HLT_GammaRAW {
          description = On the x-axis you can find all HLT gamma chains, with event counts (on y-axis). This histogram should not be empty. 
	}
	hist HLT_GammaRoIs {
          description = eta/phi map for gamma trigger RoIs. Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Egamma Expert on-call. 
	}
       }

       dir HLT_Jets {
 	output = HLT/TRHLT/HLT_Jets
	algorithm = HLT_Histogram_Not_Empty&GatherData
        hist HLT_JetsPS {
          description = On the x-axis you can find all PRESCALED HLT jet chains, with event counts (on y-axis). This histogram might be empty. 
	}
	hist HLT_JetsRAW {
          description = On the x-axis you can find all HLT jet chains, with event counts (on y-axis). This histogram should not be empty. 
	}
	hist HLT_JetsRoIs {
          description = eta/phi map for HLT jet RoIs. 
	}
       }

       dir HLT_MinBias {
 	 output = HLT/TRHLT/HLT_MinBias
	 algorithm = HLT_PassInput
	 hist HLT_MinBiasPS {
           description = On the x-axis you can find all PRESCALED HLT minbias chains, with event counts (on y-axis). This histogram might be empty. 
	 }
	 hist HLT_MinBiasRAW {
           description = On the x-axis you can find all HLT minbias chains, with event counts (on y-axis). This histogram might be empty for runs where the minbias triggers were not active. 
	 }
       }

       dir HLT_MissingET {
 	output = HLT/TRHLT/HLT_MissingET
	algorithm = HLT_Histogram_Not_Empty&GatherData
        hist HLT_MissingETPS { 
          description = On the x-axis you can find all PRESCALED HLT MET chains, with event counts (on y-axis). This histogram might be empty.
	}
	hist HLT_MissingETRAW {
          description = On the x-axis you can find all HLT MET chains, with event counts (on y-axis). This histogram should not be empty.
	}
	hist HLT_MissingETRoIs {
	  algorithm = HLT_PassInput
	  description = eta/phi map for HLT MET RoIs. 
	}
       }

       dir HLT_Muons {
 	output = HLT/TRHLT/HLT_Muons
	algorithm = HLT_Histogram_Not_Empty&GatherData
	hist HLT_MuonsPS {
          description = On the x-axis you can find all PRESCALED HLT muon chains, with event counts (on y-axis). This histogram might be empty.
	}
	hist HLT_MuonsRAW {
          description = On the x-axis you can find all HLT muon chains, with event counts (on y-axis). This histogram should not be empty.
	}
	hist HLT_MuonsRoIs {
	  description = eta/phi map for muon trigger RoIs. Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Muon Expert on-call. 
	}
       }

       dir HLT_Taus {
 	output = HLT/TRHLT/HLT_Taus
	algorithm = HLT_Histogram_Not_Empty&GatherData
        hist HLT_TausPS {
          description = On the x-axis you can find all PRESCALED HLT tau chains, with event counts (on y-axis). This histogram might be empty.
	}
	hist HLT_TausRAW {
          description = On the x-axis you can find all HLT tau chains, with event counts (on y-axis). This histogram should not be empty.
	}
	hist HLT_TausRoIs {
          description = eta/phi map for tau trigger RoIs. Histogram should not be empty. If it is, contact the Trigger Offline Expert and the Tau Expert on-call. 
	}
       }

   #end dir ResultMon
   }
#end dir HLT
}




##############
# Algorithms
##############

compositeAlgorithm HLT_Histogram_Not_Empty&GatherData {
  subalgs = GatherData,Histogram_Not_Empty
  libnames = libdqm_algorithms.so
}

algorithm HLT_Histogram_Not_Empty&GatherData {
  name = HLT_Histogram_Not_Empty&GatherData
}

algorithm HLT_Histogram_Not_Empty_with_Ref&GatherData {
  name = HLT_Histogram_Not_Empty&GatherData
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;CentrallyManagedReferences_Trigger
}


algorithm HLT_PassInput {
  libname = libdqm_algorithms.so
  name = PassInput
}

algorithm HLT_Histogram_Empty {
  libname = libdqm_algorithms.so
  name = Histogram_Empty
}




###############
# Thresholds
###############
